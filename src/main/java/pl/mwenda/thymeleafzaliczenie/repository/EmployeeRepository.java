package pl.mwenda.thymeleafzaliczenie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mwenda.thymeleafzaliczenie.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
